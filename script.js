$(document).ready(function() {
    "use strict";
    /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
    const gBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365";

    const gSMALL_COMBO = ["S", "20", "2", "200", "2", "150000"];
    const gMEDIUM_COMBO = ["M", "25", "4", "300", "3", "200000"];
    const gLARGE_COMBO = ["L", "30", "8", "500", "4", "250000"];

    const gSIZE_COMBO = 0;
    const gDUONG_KINH = 1;
    const gSUON_NUONG = 2;
    const gSALAD = 3;
    const gNUOC_NGOT = 4;
    const gGIA = 5;

    const gOCEAN_PIZZA = "ocean";
    const gHAWAII_PIZZA = "hawaiian";
    const gBACON_PIZZA = "bacon";

    var gCOMBO_PIZZA = {
        kichCo: "",
        duongKinh: "",
        suon: "",
        salad: "",
        soLuongNuoc: "",
        gia: ""
    };

    var gLOAI_PIZZA = {
        loaiPizza: ""
    }

    //khai báo biến chứa dữ liệu thu thập
    var vOrder = {
        menu: "",
        loaiPizza: "",
        loaiNuocUong: "",
        hoTen: "",
        email: "",
        soDienThoai: "",
        diaChi: "",
        loiNhan: "",
        voucher: "",
        discout: "",
        thanhToan: "",
    }

    /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
    //gọi sự kiện tải trang
    onPageLoading();

    //gán sự kiện click cho nút tạo đơn
    $("#modal-btn-confirm").on("click", function() {
        onBtnTaoDonClick();
    });

    //gán sự kiện click cho nút kiểm tra đơn
    $("#btn-order-check").on("click", function() {
        onBtnOrderCheckClick();
    });

    //gán sự kiện click cho nút chọn small
    $("#btn-small").on("click", function() {
        onBtnSmallClick();
    });

    //gán sự kiện click cho nút chọn medium
    $("#btn-medium").on("click", function() {
        onBtnMediumClick();
    });

    //gán sự kiện click cho nút chọn large
    $("#btn-large").on("click", function() {
        onBtnLargeClick();
    });

    //gán sự kiện click cho nút chọn ocean
    $("#btn-ocean").on("click", function() {
        onBtnOceanClick();
    });

    //gán sự kiện click cho nút chọn hawaii
    $("#btn-hawaii").on("click", function() {
        onBtnHawaiiClick();
    });

    //gán sự kiện click cho nút chọn bacon
    $("#btn-bacon").on("click", function() {
        onBtnBaconClick();
    });

    /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
    //hàm xử lý sự kiện tải trang
    function onPageLoading() {
        //lấy danh sách đồ uống và thêm vào select
        getDrinksList();
    }

    //hàm xử lý sự kiện bấm nút tạo đơn hàng
    function onBtnTaoDonClick() {
        $("#modal-order-info").modal("hide");
        //khai báo đối tượng chứ thông tin đơn hàng
        var vNewOrder = {
            kichCo: "",
            duongKinh: "",
            suon: "",
            salad: "",
            loaiPizza: "",
            idVourcher: "",
            thanhTien: "",
            giamGia: "",
            idLoaiNuocUong: "",
            soLuongNuoc: "",
            hoTen: "",
            email: "",
            soDienThoai: "",
            diaChi: "",
            loiNhan: "",
        }
        //B1: thu thập dữ liệu từ form
        getDataFromFormModal(vNewOrder);
        console.log(vNewOrder);
        //B2: Kiểm tra dữ liệu (k cần)
        //B3: gọi ajax tạp đơn hàng mới
        creatNewOrder(vNewOrder);
       
    }

    //hàm xử lý sự kiện bấm nút kiểm tra đơn hàng
    function onBtnOrderCheckClick() {
        //B1: thu thập dữ liệu từ form
        getDataFromForm(vOrder);
        console.log(vOrder);
        //B2: Kiểm tra dữ liệu
        var vKetQuaKiemTra = validateData(vOrder);
        if (!vKetQuaKiemTra){
            return;
        }
        //B3: hiển thị thông tin đơn hàng vào modal
        showOrderInfoInModal(vOrder);
    }

    //hàm xử lý sự kiện bấm nút small
    function onBtnSmallClick() {
        //đổi màu nút chọn
        changeButtonColor(gSMALL_COMBO[gSIZE_COMBO]);
        //lấy thông tin gói combo
        getComboPizza(gSMALL_COMBO[gSIZE_COMBO], gSMALL_COMBO[gDUONG_KINH], gSMALL_COMBO[gSUON_NUONG], gSMALL_COMBO[gSALAD], gSMALL_COMBO[gNUOC_NGOT], gSMALL_COMBO[gGIA]);
        console.log(gCOMBO_PIZZA);
    }

    //hàm xử lý sự kiện bấm nút medium
    function onBtnMediumClick() {
        //đổi màu nút chọn
        changeButtonColor(gMEDIUM_COMBO[gSIZE_COMBO]);
        //lấy thông tin gói combo
        getComboPizza(gMEDIUM_COMBO[gSIZE_COMBO], gMEDIUM_COMBO[gDUONG_KINH], gMEDIUM_COMBO[gSUON_NUONG], gMEDIUM_COMBO[gSALAD], gMEDIUM_COMBO[gNUOC_NGOT], gMEDIUM_COMBO[gGIA]);
        console.log(gCOMBO_PIZZA);
    }

    //hàm xử lý sự kiện bấm nút large
    function onBtnLargeClick() {
        //đổi màu nút chọn
        changeButtonColor(gLARGE_COMBO[gSIZE_COMBO]);
        //lấy thông tin gói combo
        getComboPizza(gLARGE_COMBO[gSIZE_COMBO], gLARGE_COMBO[gDUONG_KINH], gLARGE_COMBO[gSUON_NUONG], gLARGE_COMBO[gSALAD], gLARGE_COMBO[gNUOC_NGOT], gLARGE_COMBO[gGIA]);
        console.log(gCOMBO_PIZZA);
    }

    //hàm xử lý sự kiện bấm nút ocean
    function onBtnOceanClick() {
        //đổi màu nút chọn
        changeButtonColor(gOCEAN_PIZZA);
        //lấy thông tin loại Pizza
        getLoaiPizza(gOCEAN_PIZZA);
        console.log(gLOAI_PIZZA);
    }

    //hàm xử lý sự kiện bấm nút hawaii
    function onBtnHawaiiClick() {
        //đổi màu nút chọn
        changeButtonColor(gHAWAII_PIZZA);
        //lấy thông tin loại Pizza
        getLoaiPizza(gHAWAII_PIZZA);
        console.log(gLOAI_PIZZA);
    }

    //hàm xử lý sự kiện bấm nút bacon
    function onBtnBaconClick() {
        //đổi màu nút chọn
        changeButtonColor(gBACON_PIZZA);
        //lấy thông tin loại Pizza
        getLoaiPizza(gBACON_PIZZA);
        console.log(gLOAI_PIZZA);
    }

    /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */
    //hàm tạo đơn hàng mới
    function creatNewOrder(paramOrder) {
        //chuyển order sang json
        var vOrderJson = JSON.stringify(paramOrder);

        $.ajax({
            url: gBASE_URL + "/orders",
            type: "POST",
            data: vOrderJson,
            contentType: "application/json;charset=UTF-8",
            success: function(response) {
                console.log(response);
                //hiển thị modal thành công
                showSuccessModal(response);
            }
        });
    }

    //hàm hiển thị thông báo tạo mới thành công
    function showSuccessModal(paramOrder) {
        //ẩn modal tạo đơn
        $("#modal-order-info").modal("hide");
        //hiển thị modal thông báo thành công
        $("#modal-creat-order-success").modal("show");
        //show order code ra input
        $("#inp-order-code").val(paramOrder.orderCode);
    }

    //hàm thu thập dữ liệu từ form
    function getDataFromFormModal(paramOrder) {
        paramOrder.kichCo = vOrder.menu.kichCo;
        paramOrder.duongKinh = vOrder.menu.duongKinh;
        paramOrder.suon = vOrder.menu.suon;
        paramOrder.salad = vOrder.menu.salad;
        paramOrder.loaiPizza = vOrder.loaiPizza.loaiPizza;
        paramOrder.idVourcher = vOrder.voucher;
        paramOrder.thanhTien = vOrder.menu.gia;
        paramOrder.giamGia = vOrder.menu.gia - vOrder.thanhToan;
        paramOrder.idLoaiNuocUong = vOrder.loaiNuocUong;
        paramOrder.soLuongNuoc = vOrder.menu.soLuongNuoc;
        paramOrder.hoTen = vOrder.hoTen;
        paramOrder.email = vOrder.email;
        paramOrder.soDienThoai = vOrder.soDienThoai;
        paramOrder.diaChi = vOrder.diaChi;
        paramOrder.loiNhan = vOrder.loiNhan;
    }

    // hiển thị thông tin đơn hàng vào modal
    function showOrderInfoInModal(paramOrder) {
        //hiện modal
        $("#modal-order-info").modal("show");

        // điền dữ liệu vào input
        $("#inp-modal-full-name ").val(paramOrder.hoTen);
        $("#inp-modal-phone ").val(paramOrder.soDienThoai);
        $("#inp-modal-address ").val(paramOrder.diaChi);
        $("#inp-modal-message ").val(paramOrder.loiNhan);
        $("#inp-modal-voucher ").val(paramOrder.voucher);

        $("#tarea-modal-info ").val(
        `Xác nhận: 
        ${paramOrder.hoTen}
        SĐT: ${paramOrder.soDienThoai}
        Email: ${paramOrder.email}
        Địa chỉ: ${paramOrder.diaChi}
        ----------------------------
        Loại pizza:${paramOrder.loaiPizza.loaiPizza}
        Combo: ${paramOrder.menu.kichCo} (Đường kính: ${paramOrder.menu.duongKinh}cm, Sườn nướng : ${paramOrder.menu.suon}, Salad: ${paramOrder.menu.salad}g)
        Nước ngọt: ${paramOrder.loaiNuocUong}
        Giá: ${paramOrder.menu.gia}đ
        ----------------------------
        Mã giảm giá: ${paramOrder.voucher}
        Giảm giá: ${paramOrder.discout}%
        Phải thanh toán: ${paramOrder.thanhToan}đ
        Lời nhắn: ${paramOrder.loiNhan}`
        );
    }

    //hàm kiểm tra dữ liệu 
    function validateData(paramOrder) {
        if(paramOrder.menu.kichCo == ""){
            alert ("Vui lòng chọn Combo pizza !");
            return false;
        }
        if(paramOrder.loaiPizza.loaiPizza == ""){
            alert ("Vui lòng chọn loại pizza !");
            return false;
        }
        if(paramOrder.loaiNuocUong == 0){
            alert ("Vui lòng chọn loại nước uống !");
            return false;
        }
        if(paramOrder.hoTen == ""){
            alert ("Vui lòng nhập Họ và tên !");
            return false;
        }
        if(paramOrder.email == ""){
            alert ("Vui lòng nhập Email !");
            return false;
        }
        if(paramOrder.soDienThoai == ""){
            alert ("Vui lòng nhập Số điện thoại !");
            return false;
        }
        if(paramOrder.diaChi == ""){
            alert ("Vui lòng nhập địa chỉ !");
            return false;
        }
        
        //kiểm tra tính hơp lệ của email
        if (!isEmail(paramOrder.email)) {
            alert ("Email nhập vào không hợp lệ !");
            return false;
        }

        return true;
    }

    //hàm kiểm tra tính hơp lệ của email
    function isEmail(paramEmail) {
        return String(paramEmail).toLowerCase().match(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    }

    //hàm thu thập dữ liệu từ form
    function getDataFromForm(paramOrder) {
        paramOrder.menu = gCOMBO_PIZZA;
        paramOrder.loaiPizza = gLOAI_PIZZA;
        paramOrder.loaiNuocUong = $("#select-drinks").val();
        paramOrder.hoTen = $("#inp-fullname").val().trim();
        paramOrder.email = $("#inp-email ").val().trim();
        paramOrder.soDienThoai = $("#inp-dien-thoai").val().trim();
        paramOrder.diaChi = $("#inp-dia-chi ").val().trim();
        paramOrder.loiNhan = $("#inp-message ").val().trim();
        paramOrder.voucher = $("#inp-voucher-id ").val().trim();
        paramOrder.discout = getDiscountByVoucher(paramOrder.voucher);
        paramOrder.thanhToan = paramOrder.menu.gia * (1 - paramOrder.discout / 100);
    }

    //hàm lấy discout từ mã voucher
    function getDiscountByVoucher(paramVoucher) {
        var vDiscout = 0;
        if (paramVoucher != "") {
            $.ajax({
                url: gBASE_URL + "/voucher_detail/" + paramVoucher,
                type: "GET",
                async: false,
                success: function(response) {
                    console.log(response);
                    vDiscout = response.phanTramGiamGia;
                },
                error: function(error) {
                    alert ("Không tigm thấy mã giảm giá: " + paramVoucher);
                }
            });
        }
        return vDiscout;
    }

    //hàm lấy danh sách đồ uống và thêm vào select
    function getDrinksList() {
        //gọi ajax lấy danh sách đồ uống
        $.ajax({
            url: gBASE_URL + "/drinks",
            type: "GET",
            success: function(response) {
                console.log(response);
                // thêm vào đồ uống vào select
                insertDrinksInSelect(response);
            },
            error: function(error) {
                console.log(error.responseText);
            }
        });
    }

    //hàm thêm vào đồ uống vào select
    function insertDrinksInSelect(paramDrinks) {
        var vSeclect = $("#select-drinks");
        paramDrinks.map(drink => {
            $("<option>", {
                value: drink.maNuocUong,
                html: drink.tenNuocUong
            }).appendTo(vSeclect);
        });
    }

    //hàm lấy thông tin loại pizza
    function getLoaiPizza(paramLoaiPizza) {
        //lấy data is-selected-pizza
        var vDataOcean = $("#btn-ocean").data("is-selected-pizza");
        var vDataHawaii = $("#btn-hawaii").data("is-selected-pizza");
        var vDataBacon = $("#btn-bacon").data("is-selected-pizza");

        if (vDataOcean === "Y") {
            gLOAI_PIZZA.loaiPizza = paramLoaiPizza;
        }
        if (vDataHawaii === "Y") {
            gLOAI_PIZZA.loaiPizza = paramLoaiPizza;
        }
        if (vDataBacon === "Y") {
            gLOAI_PIZZA.loaiPizza = paramLoaiPizza;
        }
    }

    //hàm lấy thông tin gói combo
    function getComboPizza (paramSize, paramDuongKinh, paramSuon, paramSalad, paramNuocNgot, paramGia) {
        //lấy data is-selected-pizza
        var vDataSmall = $("#btn-small").data("is-selected-pizza");
        var vDataMedium = $("#btn-medium").data("is-selected-pizza");
        var vDataLarge = $("#btn-large").data("is-selected-pizza");

        if (vDataSmall === "Y") {
            gCOMBO_PIZZA.kichCo = paramSize;
            gCOMBO_PIZZA.duongKinh = paramDuongKinh;
            gCOMBO_PIZZA.suon = paramSuon;
            gCOMBO_PIZZA.salad = paramSalad;
            gCOMBO_PIZZA.soLuongNuoc = paramNuocNgot;
            gCOMBO_PIZZA.gia = paramGia;
        }
        if (vDataMedium === "Y") {
            gCOMBO_PIZZA.kichCo = paramSize;
            gCOMBO_PIZZA.duongKinh = paramDuongKinh;
            gCOMBO_PIZZA.suon = paramSuon;
            gCOMBO_PIZZA.salad = paramSalad;
            gCOMBO_PIZZA.soLuongNuoc = paramNuocNgot;
            gCOMBO_PIZZA.gia = paramGia;
        }
        if (vDataLarge === "Y") {
            gCOMBO_PIZZA.kichCo = paramSize;
            gCOMBO_PIZZA.duongKinh = paramDuongKinh;
            gCOMBO_PIZZA.suon = paramSuon;
            gCOMBO_PIZZA.salad = paramSalad;
            gCOMBO_PIZZA.soLuongNuoc = paramNuocNgot;
            gCOMBO_PIZZA.gia = paramGia;
        }
    }

    //hàm đổi màu nút "chọn" khi được bấm
    function changeButtonColor(paramButton) {
        //thay đổi màu cút chọn combo
        if (paramButton === gSMALL_COMBO[gSIZE_COMBO]) {
            $("#btn-small")
                .prop("class", "btn btn-success w-100") //thay đổi class nút được chọn màu xanh
                .data("is-selected-pizza", "Y"); //set data nút đc chọn thành Y

            $("#btn-medium")
                .prop("class", "btn btn-warning w-100") //thay đổi class nút không được chọn màu cam
                .data("is-selected-pizza", "N"); //set data nút không được chọn thành N

            $("#btn-large")
                .prop("class", "btn btn-warning w-100") //thay đổi class nút không được chọn màu cam
                .data("is-selected-pizza", "N"); //set data nút không được chọn thành N
        }
        if (paramButton === gMEDIUM_COMBO[gSIZE_COMBO]) {
            $("#btn-small")
                .prop("class", "btn btn-warning w-100") //thay đổi class nút không được chọn màu cam
                .data("is-selected-pizza", "N"); //set data nút không được chọn thành N

            $("#btn-medium")
                .prop("class", "btn btn-success w-100") //thay đổi class nút được chọn màu xanh
                .data("is-selected-pizza", "Y"); //set data nút đc chọn thành Y

            $("#btn-large")
                .prop("class", "btn btn-warning w-100") //thay đổi class nút không được chọn màu cam
                .data("is-selected-pizza", "N"); //set data nút không được chọn thành N
        }
        if (paramButton === gLARGE_COMBO[gSIZE_COMBO]) {
            $("#btn-small")
                .prop("class", "btn btn-warning w-100") //thay đổi class nút không được chọn màu cam
                .data("is-selected-pizza", "N"); //set data nút không được chọn thành N

            $("#btn-medium")
                .prop("class", "btn btn-warning w-100") //thay đổi class nút không được chọn màu cam
                .data("is-selected-pizza", "N"); //set data nút không được chọn thành N

            $("#btn-large")
                .prop("class", "btn btn-success w-100") //thay đổi class nút được chọn màu xanh
                .data("is-selected-pizza", "Y"); //set data nút đc chọn thành Y
        }

        //thay đổi màu nút chọn loại pizza
        if (paramButton === gOCEAN_PIZZA) {
            $("#btn-ocean")
                .prop("class", "btn btn-success w-100") //thay đổi class nút được chọn màu xanh
                .data("is-selected-pizza", "Y"); //set data nút đc chọn thành Y

            $("#btn-hawaii")
                .prop("class", "btn btn-warning w-100") //thay đổi class nút không được chọn màu cam
                .data("is-selected-pizza", "N"); //set data nút không được chọn thành N

            $("#btn-bacon")
                .prop("class", "btn btn-warning w-100") //thay đổi class nút không được chọn màu cam
                .data("is-selected-pizza", "N"); //set data nút không được chọn thành N
        }
        if (paramButton === gHAWAII_PIZZA) {
            $("#btn-ocean")
                .prop("class", "btn btn-warning w-100") //thay đổi class nút không được chọn màu cam
                .data("is-selected-pizza", "N"); //set data nút không được chọn thành N

            $("#btn-hawaii")
                .prop("class", "btn btn-success w-100") //thay đổi class nút được chọn màu xanh
                .data("is-selected-pizza", "Y"); //set data nút đc chọn thành Y

            $("#btn-bacon")
                .prop("class", "btn btn-warning w-100") //thay đổi class nút không được chọn màu cam
                .data("is-selected-pizza", "N"); //set data nút không được chọn thành N
        }
        if (paramButton === gBACON_PIZZA) {
            $("#btn-ocean")
                .prop("class", "btn btn-warning w-100") //thay đổi class nút không được chọn màu cam
                .data("is-selected-pizza", "N"); //set data nút không được chọn thành N

            $("#btn-hawaii")
                .prop("class", "btn btn-warning w-100") //thay đổi class nút không được chọn màu cam
                .data("is-selected-pizza", "N"); //set data nút không được chọn thành N

            $("#btn-bacon")
                .prop("class", "btn btn-success w-100") //thay đổi class nút được chọn màu xanh
                .data("is-selected-pizza", "Y"); //set data nút đc chọn thành Y
        }
    }
});